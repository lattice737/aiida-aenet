
def setup_package():
    """Set up the package"""
    import json
    from setuptools import setup, find_packages

    with open('setup.json', 'r') as handle:
        setup_json = json.load(handle)

    setup(
        include_package_data=True,
        packages=find_packages(),
        **setup_json
    )

if __name__ == '__main__':
    setup_package()

