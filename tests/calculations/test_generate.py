# No need to import fixtures here - they are added by pytest "automagically"


def test_qe_calculation(
    aiida_local_code_factory, integer_input
):  # requesting "aiida_local_code_factory" and "integer_input" fixtures
    """Test running a calculation using a CalcJob plugin."""

    from aiida.engine import run
    from aiida_aenet.calculations.generate import AenetGenerateCalculation

    # search for 'pw.x' executable in PATH, set up an AiiDA code for it and return it
    code = aiida_local_code_factory(entry_point='metallicglass.generate',
                                    executable='pw.x')

    inputs = {"code": code, "reference": List(list=[]), "potential": potential}

    # run a calculation using this code ...
    result = run(AenetGenerateCalculation, **inputs)

    # check outputs of calculation
    assert result['...'] == ...