from aiida.orm import load_node
from aiida.engine import submit

from aiida_aenet.calculations.predict import AenetPredictCalculation

generate_calc = load_node(11306)
train_calc = load_node(11310)

inputs = {
    "code": load_node(10740),
    "reference": generate_calc.inputs.reference,
    "algorithm": train_calc.inputs.algorithm,
    "potential": train_calc.outputs.potential,
}

calc_node = submit(AenetPredictCalculation, **inputs)